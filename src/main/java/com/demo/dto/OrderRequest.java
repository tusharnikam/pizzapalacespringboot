package com.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class OrderRequest {

    private Integer customerId;
    private String deliveryAddress;
    private List<PizzaRequest> pizzas;
}
