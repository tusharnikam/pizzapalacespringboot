package com.demo.entity;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderLine {

    private  Integer orderId;
    private Integer pizzaId;
    private  Integer orderLineId;
    private String size;
    private Integer quantity;
    private Integer totalPrice;


}
