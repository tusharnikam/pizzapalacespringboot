package com.demo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Orders {


    private Integer customerId;
    private Integer orderId;
    private String status;
    private Integer totalAmount;
    private String orderDateTime;
    private  String deliveryAddress;
    private List<Pizzas> pizzas;





}
