package com.demo.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Pizzas {

    private Integer pizzaId;


    private String name;


    private String description;


    private String type;


    private String imageUrl;


    private Integer priceRegularSize;


    private Integer priceMediumSize;


    private Integer priceLargeSize;

    private Integer quantity;

    private String size;

    private List<OrderLine> orderLine;

    private Integer price;



}
