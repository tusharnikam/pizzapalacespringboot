package com.demo.controller;

import com.demo.entity.Orders;
import com.demo.response.OrderResponse;
import com.demo.response.OrderResponseList;
import com.demo.response.ResponseUtil;
import com.demo.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@Slf4j
public class OrderController {

   @Autowired
   OrderService orderService;


    @PostMapping("/orders")
    public ResponseEntity<OrderResponse> createOrder(@RequestBody Orders orders) {
        log.info("inside create order function");
            OrderResponse orderResponse= orderService.createOrder(orders);

            return  new ResponseEntity<>(orderResponse, HttpStatus.CREATED);


    }

    @GetMapping("/orders")
    public ResponseEntity<OrderResponseList> getAllOrders() {
       log.info("inside getAllOrders method ");

        OrderResponseList orderResponse=orderService.getAllOrders();

        return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }
@GetMapping("/orders/{orderId}")
    public ResponseEntity<Object> getOrderById(@PathVariable Integer orderId){
    log.info("inside getOrderById  method");
      Orders orders=orderService.getOrderById(orderId);
        return ResponseUtil.successResponse(true,"successfully fetch order details", orders);
    }

    @DeleteMapping("/orders/{orderId}")
    public String deleteOrder(@PathVariable Integer orderId){
        log.info("inside deleteOrder  method");

        orderService.deleteOrder(orderId);
        return "order deleted successfully";
    }
@PutMapping("/orders/{orderId}")
    public ResponseEntity<OrderResponse> updateOrderDetails(@PathVariable Integer orderId, @RequestBody Orders orders){
    log.info("inside updateOrderDetails  method");

        orders.setOrderId(orderId);
        OrderResponse orderResponse=orderService.updateOrderDetails(orders);
        log.info("Successfully update order details ");
    return new ResponseEntity<>(orderResponse, HttpStatus.OK);
    }


}
