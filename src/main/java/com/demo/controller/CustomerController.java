package com.demo.controller;

import com.demo.entity.Customer;
import com.demo.exception.ErrorMessageResponse;
import com.demo.response.ResponseUtil;
import com.demo.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Slf4j
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@ComponentScan
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
        log.info("Inside createCustomer method");
        Integer id = customerService.createCustomer(customer);
        log.info("Successfully created customer ");
        return ResponseUtil.successResponse(true, "successfully created customer", customer);

    }

    @GetMapping("/customers/{CustomerId}")
    public ResponseEntity<Object> getCustomerDetails(@PathVariable("CustomerId") Integer customerId) {
        log.info("Inside get customer detail method");
        Customer customer = customerService.getCustomerDetails(customerId);
        if (customer == null) {
        ErrorMessageResponse response = ErrorMessageResponse.builder().code(400).messaage("Not Found").
        build();
        return ResponseUtil.errorResponse(false, "Failed to fetched customer data", response);
        } else {
        log.info("successfully fetched customer detail");
        return ResponseUtil.successResponse(true, "successfully fetched customer detail", customer);
        }
    }

    @GetMapping("/customers")
    public ResponseEntity<Object> getAllCustomerDetails() {
        log.info("Inside get all customer details method");
        List<Customer> customers = customerService.getAllCustomerDetails();
        if (customers == null) {
        ErrorMessageResponse response = ErrorMessageResponse.builder().code(500).messaage("Failed to fetched customer data")
        .build();

        return ResponseUtil.errorResponse(false, "Failed to fetched customer data", response);
        } else {
        log.info("successfully fetched customer detail");
        return ResponseUtil.successResponse(true, "successfully fetched customer detail", customers);
        }
    }


    @PutMapping("/customers/{customerId}")
    public ResponseEntity<Object> updateCustomerDetails(@PathVariable("customerId") Integer customerId, @RequestBody Customer customer) {
        log.info("Inside update customer details method");
        customer.setCustomerId(customerId);
        customerService.updateCustomerDetails(customer);
        log.info("Successfully updated customer");
        return ResponseUtil.successResponse(true, "successfully updated customer", customer);
    }


    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable("customerId") Integer customerId) {
        log.info("Inside delete customer method");
        boolean response=  customerService.deleteCustomer(customerId);
        if (!response)
        {
        ErrorMessageResponse response1 = ErrorMessageResponse.builder().code(500).messaage("Invalid Customer ID").build();
        return ResponseUtil.errorResponse(false, "Failed to delete customer", response1);
        }
        else {
        return ResponseUtil.successResponse(true, "successfully deleted customer", "Deleted id is "+customerId);
        }


    }
}
