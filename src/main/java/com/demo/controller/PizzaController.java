package com.demo.controller;

import com.demo.entity.Pizzas;
import com.demo.response.ResponseUtil;
import com.demo.service.PizzaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@ComponentScan
@CrossOrigin(origins = "http://localhost:3000")
public class PizzaController {

    @Autowired
    private PizzaService pizzaService;


    @PostMapping("/pizzas")
    public ResponseEntity<Object> createPizzas (@RequestBody Pizzas pizza) {
        log.info("inside create pizza method");
        pizzaService.createPizzas(pizza);
        return ResponseUtil.successResponse(true ,"successfully created pizza",pizza);

    }

    @GetMapping("/pizzas/{id}")
    public ResponseEntity<Object> getPizzaDetails(@PathVariable("id") Integer pizzaId) {
        log.info("inside getPizzaDetails method");
        Pizzas pizza= pizzaService.getPizzaDetails(pizzaId);
        return ResponseUtil.successResponse(true ,"successfully fetched pizza detail",pizza);

    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/pizzas")
    public ResponseEntity<Object> getAllPizzas() {
        log.info("inside getAllPizzas method");
        List<Pizzas> pizzas= pizzaService.getAllPizzas();
        return ResponseUtil.successResponse(true ,"successfully fetched all pizza details",pizzas);

    }

    @PutMapping("/pizzas/{pizzaId}")
    public ResponseEntity<Object> updatePizzaDetails(@PathVariable int pizzaId,@RequestBody Pizzas pizza){
        log.info("inside updatePizzaDetails method");
        pizza.setPizzaId(pizzaId);
         pizzaService.updatePizzaDetails(pizza);
        return ResponseUtil.successResponse(true ,"successfully updated pizza detail",pizza);

    }

    @DeleteMapping("/pizzas/{id}")
    public String deletePizza(@PathVariable("id") Integer id) {
        log.info("inside deletePizza method");

        pizzaService.deletePizza(id);

        return "Removed successfully!!";
    }




}
