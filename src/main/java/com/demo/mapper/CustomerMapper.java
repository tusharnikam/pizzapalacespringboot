package com.demo.mapper;

import com.demo.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CustomerMapper {
  public Integer createCustomer(@Param("customer") Customer customer);

  public Customer getCustomerDetails(Integer customerId);

 public List<Customer> getAllCustomerDetails();

 public void updateCustomerDetails(Customer customer);

 public boolean deleteCustomer(Integer customerId);
}
