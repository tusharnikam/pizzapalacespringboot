package com.demo.mapper;

import com.demo.entity.Orders;
import com.demo.entity.Pizzas;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrderMapper {
    public Integer  createOrder(@Param("orders") Orders orders);

    public List<Orders> getAllOrders();

    public void deleteOrder(Integer orderId);

    public Orders getOrderById(Integer orderId);
    public void updateOrderDetails( Orders orders);
}
