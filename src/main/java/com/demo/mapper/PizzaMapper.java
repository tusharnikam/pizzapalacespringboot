package com.demo.mapper;

import com.demo.entity.Pizzas;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface PizzaMapper {

    public void createPizza(@Param("pizza")Pizzas pizza);

   public  Pizzas getPizzaDetails(Integer pizzaId);

   public List<Pizzas> getAllPizzas();

   public void updatePizzaDetails(Pizzas pizza);

   public   void deletePizza(Integer id);
}
