package com.demo.service;

import com.demo.entity.OrderLine;
import com.demo.entity.Orders;
import com.demo.entity.Pizzas;
import com.demo.mapper.CustomerMapper;
import com.demo.mapper.OrderLineMapper;
import com.demo.mapper.OrderMapper;
import com.demo.mapper.PizzaMapper;
import com.demo.response.OrderResponse;
import com.demo.response.OrderResponseList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderLineMapper orderLineMapper;


    public OrderResponse createOrder(Orders order) {


        Orders createdOrder = Orders.builder().orderDateTime(String.valueOf(new Date())).customerId(order.getCustomerId())
                .status("Pending").totalAmount(order.getTotalAmount()).deliveryAddress(order.getDeliveryAddress())
                .orderId(order.getOrderId()).build();


        Integer orderId = orderMapper.createOrder(createdOrder);


        for (Pizzas pizza : order.getPizzas()) {
            // Create an OrderLine entity for each pizza
            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(createdOrder.getOrderId());
            orderLine.setPizzaId(pizza.getPizzaId());
            orderLine.setQuantity(pizza.getQuantity());
            orderLine.setSize(pizza.getSize());
            orderLine.setTotalPrice(pizza.getPrice());

            orderLineMapper.createOrderLine(orderLine);
        }

        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order Created SuccesFully").order(createdOrder).Data(order.getPizzas()).build();

        return orderResponse;

    }

    public OrderResponseList getAllOrders() {
        List<Orders> allOrders = orderMapper.getAllOrders();

        OrderResponseList orderResponseList = (OrderResponseList) OrderResponseList.builder().success(true)
                .message("successfully fetched all orders").Data(allOrders).build();
        return orderResponseList;

    }

    public void deleteOrder(Integer orderId) {
        orderMapper.deleteOrder(orderId);
    }


    public Orders getOrderById(Integer orderId) {
        Orders orders = orderMapper.getOrderById(orderId);
        return orders;
    }


    public OrderResponse updateOrderDetails(Orders orders) {
        Orders.builder().status("Pending").totalAmount(orders.getTotalAmount()).deliveryAddress(orders.getDeliveryAddress()).orderId(orders.getOrderId()).build();
        orderMapper.updateOrderDetails(orders);
        for (Pizzas pizza : orders.getPizzas()) {
            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(orders.getOrderId());
            orderLine.setPizzaId(pizza.getPizzaId());
            orderLine.setQuantity(pizza.getQuantity());
            orderLine.setSize(pizza.getSize());
            orderLine.setTotalPrice(pizza.getPrice());
            orderLineMapper.updateOrderLine(orderLine);
            System.out.println("order updated in orderline");
        }
        OrderResponse orderResponse = OrderResponse.builder().success(true).message("Order updated Successfully").order(orders).build();
        return orderResponse;
    }
}