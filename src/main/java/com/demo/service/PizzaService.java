package com.demo.service;

import com.demo.entity.Pizzas;
import com.demo.mapper.PizzaMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PizzaService {

    @Autowired
    private PizzaMapper pizzaMapper;

    public void createPizzas(@Param("pizza")Pizzas pizza) {

        pizzaMapper.createPizza(pizza);

    }

    public Pizzas getPizzaDetails(Integer pizzaId) {

        return pizzaMapper.getPizzaDetails(pizzaId);
    }

    public List<Pizzas> getAllPizzas() {

        return pizzaMapper.getAllPizzas();
    }

    public Pizzas updatePizzaDetails(Pizzas pizza) {

        pizzaMapper.updatePizzaDetails(pizza);
        return pizzaMapper.getPizzaDetails(pizza.getPizzaId());
    }

    public void deletePizza(Integer id) {

        pizzaMapper.deletePizza(id);
    }
}
