package com.demo.service;

import com.demo.entity.Customer;
import com.demo.mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerMapper customerMapper;


    public Integer createCustomer(Customer customer) {
        Integer id = customerMapper.createCustomer(customer);
        return id;

    }

    public Customer getCustomerDetails(Integer customerId) {

        return customerMapper.getCustomerDetails(customerId);
    }

    public List<Customer> getAllCustomerDetails() {
        return customerMapper.getAllCustomerDetails();


    }

    public Customer updateCustomerDetails(Customer customer) {
        customerMapper.updateCustomerDetails(customer);
        return customerMapper.getCustomerDetails(customer.getCustomerId());
    }

    public boolean deleteCustomer(Integer customerId) {

       boolean response= customerMapper.deleteCustomer(customerId);
        return response;
    }
}
